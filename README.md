# to run
clone\
cd threejsocclusionculling\
http-server -p 8001\
browser: localhost:8001/demo_movement.html

# 1st iteration
[![name](pics/PointCloudThree.js.jpg)](https://docs.google.com/presentation/d/1la-ohelvDEJnELARkhPbyMaNXxw6rpjWiX7U7bkvM3g/edit?usp=sharing)

# 2nd iteration
[![name](pics/glb_model_handling_occlusions.png)](https://docs.google.com/presentation/d/12SGARoG_T2tbPGKS5CafZvkvyJMch35VIBlxDo3GCq4/edit#slide=id.p)

# 3rd iteration
[![name](pics/sumulating_lidar_scan.png)](https://docs.google.com/presentation/d/12SGARoG_T2tbPGKS5CafZvkvyJMch35VIBlxDo3GCq4/edit#slide=id.g11e02660ede_0_1)

# 4rth iteration
[![name](pics/simulator.png)]