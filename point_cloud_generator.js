// console.log(math);

// import * as THREE from './build/three.module.js';
// import { OrbitControls } from './examples/jsm/controls/OrbitControls.js';
// import { GLTFLoader } from './examples/jsm/loaders/GLTFLoader.js';
// import { CanvasTexture } from './src/textures/CanvasTexture.js';
// import { SVGLoader } from './examples/jsm/loaders/SVGLoader.js';
// import { TransformControls } from './examples/jsm/controls/TransformControls.js';
// import { Line2 } from './examples/jsm/lines/Line2.js';
// import { LineMaterial } from './examples/jsm/lines/LineMaterial.js';
// import { LineGeometry } from './examples/jsm/lines/LineGeometry.js';
// import { Wireframe } from './examples/jsm/lines/Wireframe.js';
// import { WireframeGeometry2 } from './examples/jsm/lines/WireframeGeometry2.js';
// import { Sky } from './examples/jsm/objects/Sky.js';
// import { FontLoader } from './examples/jsm/loaders/FontLoader.js';
// import { TextGeometry } from './examples/jsm/geometries/TextGeometry.js';
// import { PointerLockControls } from './examples/jsm/controls/PointerLockControls.js';
// import { Util } from './util.js';


import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.120.0/build/three.module.js';
import { OrbitControls } from 'https://cdn.jsdelivr.net/npm/three@0.120.0/examples/jsm/controls/OrbitControls.js';
import { GLTFLoader } from "https://cdn.jsdelivr.net/npm/three@0.120.0/examples/jsm/loaders/GLTFLoader.js";


console.log("threejs revision", THREE.REVISION);

let PointCloudGenerator = class {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }

    // Loads GLTF models
    loadModelData(gltloader, url) {
        return new Promise((resolve, reject) => {
            gltloader.load(url, data => resolve(data), null, reject);
        });
    }

    setColor = function (obj, color) {
        obj.children.forEach((child) => {
            this.setColor(child, color);
        });
        if (obj.material) {
            obj.material.color = color;
        };
    };

    setOpacity = function (obj, opacity) {
        this.test();

        obj.children.forEach((child) => {
            this.setOpacity(child, opacity);
            this.test();
        });
        if (obj.material) {
            obj.material.transparent = true;
            obj.material.opacity = opacity;
        };
    };

    test = function () {
        return true;
    }


    isPointVisible = function (sensor_origin, pt, mesh) {
        const v = new THREE.Vector3();
        v.copy(pt.point).sub(sensor_origin).normalize();
        const raycaster = new THREE.Raycaster(sensor_origin, v, 0, pt.distance);

        if (mesh.children.length == 1) {
            for (let m = 0; m < mesh.children[0].children.length; m++) {
                let intersections = raycaster.intersectObject(mesh.children[0].children[m]);

                if (intersections.length > 0)
                    return false;
                else
                    return true;
            };
        } else return false;
    };

    // Recursively set transparency.
    setNameAll(group, name) {

        group.traverse(function (child) {

            child.name = name;
        });
    }


    // make a group out of a GLB file and recenter if a recenter parent object is provided
    makeGroup(gltf, params) {
        // console.log(params);
        let group = new THREE.Group(gltf);
        group.name = params.name;
        group.add(gltf.scene);

        this.setNameAll(group, params.name);

        group.position.set(params.position[0], params.position[1], params.position[2]);

        if (params.rotation) {
            group.rotation.x = params.rotation[0]
            group.rotation.y = params.rotation[1]
            group.rotation.z = params.rotation[2]
        }

        // console.log(group);
        // console.log(params.recenter.parent);

        if (Object.keys(params.recenter).length > 0 && params.recenter.recenter) {
            const aabb = new THREE.Box3();
            aabb.setFromObject(params.recenter.parent);

            group.scale.multiplyScalar(params.scale);

            const aabb2 = new THREE.Box3();
            aabb2.setFromObject(group);

            this.test();

            group.position.add(aabb.center().add(aabb2.center().multiplyScalar(-1)));
        } else {
            group.scale.multiplyScalar(params.scale);
        }

        this.setOpacity(group, params.opacity);

        if (params.color)
            this.setColor(group, new THREE.Color(params.color));

        return group;
    }

    // constructs a mesh grid by checking intersections between horizontal and vertical lines
    GridGeometry(width = 1, height = 1, grid_density, grid_origin, lExt = [0, 0]) {
        let wSeg = width * grid_density;
        let hSeg = height * grid_density;

        let seg = new THREE.Vector2(width / wSeg, height / hSeg);

        let horiz_segment = [];
        for (let y = 0; y <= hSeg; y++) {
            horiz_segment.push([new THREE.Vector3(0, y * seg.y, 0),
            new THREE.Vector3(width + (seg.x * lExt[0]), y * seg.y, 0)]);
        }

        let vert_segment = [];
        for (let x = 0; x <= wSeg; x++) {
            vert_segment.push([new THREE.Vector3(x * seg.x, 0, 0),
            new THREE.Vector3(x * seg.x, height + (seg.y * lExt[1]), 0)]);
        }

        //intersections using mathjs
        let meshgrid = [];
        horiz_segment.forEach(horiz_pair => {
            vert_segment.forEach(vert_pair => {
                this.test();
                let intersection = math.intersect(
                    [horiz_pair[0].x, horiz_pair[0].y, horiz_pair[0].z], [horiz_pair[1].x, horiz_pair[1].y, horiz_pair[1].z],
                    [vert_pair[0].x, vert_pair[0].y, vert_pair[0].z], [vert_pair[1].x, vert_pair[1].y, vert_pair[1].z]);

                intersection = new THREE.Vector3(intersection[0], intersection[1], intersection[2]).add(grid_origin);
                intersection = [intersection.x, intersection.y, intersection.z];

                meshgrid.push(intersection);
            });
        });

        return meshgrid;
    }


    find_visible_intersection_points(all_objects, grid_point, grid_index, dir, known_object_names) {
        let visible_point_data = [];

        let visible_interaction_points = [];

        all_objects.forEach(object => {

            let group = object.object_outer;
            let group_ = object.object_inner;

            group.children[0].children.forEach(child => {

                // console.log(child);

                let raycaster = new THREE.Raycaster(grid_point, dir, 0, 9999);
                let ray_intersections = raycaster.intersectObject(child);

                // if (scene !== null) {
                //     if (grid_index % 100 === 0) {
                //         // console.log("adding rays");
                //         let rays = new THREE.ArrowHelper(raycaster.ray.direction, raycaster.ray.origin, 1000, '#' + Math.random().toString(16).substr(-6), 0, 0)
                //         scene.add(rays);
                //     }
                // }

                if (ray_intersections.length > 0) {
                    for (let [ray_intersection_index, ray_intersection] of ray_intersections.entries()) {
                        // console.log('intersection');
                        // check if a point is visible or occluded
                        if (this.isPointVisible(grid_point, ray_intersection, group_)) {

                            let visible = true;

                            all_objects.forEach(object_ => {

                                if (object_.object_inner !== group_ && !this.isPointVisible(grid_point, ray_intersection, object_.object_inner)) {

                                    visible = false;

                                }
                            });

                            // console.log(ray_intersection.object.name);

                            if (visible) {

                                visible_point_data.push([ray_intersection.point.x, ray_intersection.point.y, ray_intersection.point.z,
                                                        ray_intersection.object.material.color.r * 255, ray_intersection.object.material.color.g * 255, ray_intersection.object.material.color.b * 255,
                                                        ray_intersection.face.normal.x, ray_intersection.face.normal.y, ray_intersection.face.normal.z,
                                                        ray_intersection.object.material.roughness,
                                                        known_object_names.indexOf(ray_intersection.object.name)]);

                                //visible_point_data.push([ray_intersection.point.x, ray_intersection.point.y, ray_intersection.point.z,
                                //    known_object_names.indexOf(ray_intersection.object.name)]);

                                    

                                //add the point to the scene
                                let intersection_point_ = new THREE.Points(new THREE.BufferGeometry().setFromPoints([ray_intersection.point]), new THREE.PointsMaterial({ color: 0xff0000, size: 0.3 }));
                                intersection_point_.name = "grid_point_" + grid_index + "_visible_intersection_point_" + ray_intersection_index

                                visible_interaction_points.push(intersection_point_);
                            }
                        }
                    }
                }
            });
        });

        return [visible_interaction_points, visible_point_data]
    }
};

export { PointCloudGenerator };