import pandas as pd
import open3d as o3d
import numpy as np
import trimesh
import os
import scipy.io
from PIL import Image

root = '/Users/borismocialov/Downloads/hoang_tasks/simple/missing_silo_lower_resolution/'
#'/Users/borismocialov/Downloads/Stanford3dDataset_v1.2/Area_1/'
#'/Users/borismocialov/Downloads/kitchens/w2_9f_kitchen_01/' 
#'/Users/borismocialov/Downloads/toilets/w2_2f_toilet_01/' 
#'/Users/borismocialov/Downloads/IJRR-Dataset-1-subset/' 
#'/Users/borismocialov/Downloads/'

sub = 'modified'
#'conferenceRoom_1' 
#'kitchen_scan' 
#'toilet_scan' 
#'SCANS_reduced' 
#'SF_40_FOV_10'


point_cloud_scan_files = [filename for filename in os.listdir(root+sub+'/') if not filename.startswith(".")]

full_scan = None

for point_cloud_scan_file in point_cloud_scan_files:
    print (point_cloud_scan_file)
    if point_cloud_scan_file.split('.')[-1] == 'csv':
        df = pd.read_csv(root+sub+'/'+point_cloud_scan_file, header=None, sep=' ')
        new_scan = df[[df.columns[0], df.columns[1], df.columns[2], df.columns[10], df.columns[6], df.columns[7], df.columns[8]]].values
    elif point_cloud_scan_file.split('.')[-1] == 'mat':
        df = scipy.io.loadmat(root+sub+'/'+point_cloud_scan_file)
        new_scan = np.concatenate((df['SCAN']['XYZ'][0][0][0].reshape(-1,1), 
                        df['SCAN']['XYZ'][0][0][1].reshape(-1,1),
                        df['SCAN']['XYZ'][0][0][2].reshape(-1,1),
                        np.array([0]*len(df['SCAN']['XYZ'][0][0][0])).reshape(-1,1),
                        np.array([0]*len(df['SCAN']['XYZ'][0][0][0])).reshape(-1,1),
                        np.array([0]*len(df['SCAN']['XYZ'][0][0][0])).reshape(-1,1),
                        np.array([0]*len(df['SCAN']['XYZ'][0][0][0])).reshape(-1,1)), axis=1)
    elif point_cloud_scan_file.split('.')[-1] == 'pcd':
        df = o3d.io.read_point_cloud(root+sub+'/'+point_cloud_scan_file)
        new_scan = np.concatenate((np.asarray(df.points)[:,0].reshape(-1,1), 
                        np.asarray(df.points)[:,1].reshape(-1,1),
                        np.asarray(df.points)[:,2].reshape(-1,1),
                        np.array([0]*len(np.asarray(df.points))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df.points))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df.points))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df.points))).reshape(-1,1)), axis=1)
    elif point_cloud_scan_file.split('.')[-1] == 'ppm' or point_cloud_scan_file.split('.')[-1] == 'png':
        df = np.array(Image.open(root+sub+'/'+point_cloud_scan_file))
        df_ = []
        height = df.shape[0]
        width = df.shape[1]
        for i in range(height):
            for j in range(width):
                depth_value = df[i, j]
                if type(depth_value) == np.ndarray:
                    #rgb
                    df_.append(np.array([i,j,depth_value[0]]))
                else:
                    df_.append(np.array([i,j,depth_value]))
        df = np.array(df_)
        print (df.shape)
        new_scan = np.concatenate((np.asarray(df)[:,0].reshape(-1,1), 
                        np.asarray(df)[:,1].reshape(-1,1),
                        np.asarray(df)[:,2].reshape(-1,1),
                        np.array([0]*len(np.asarray(df))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df))).reshape(-1,1),
                        np.array([0]*len(np.asarray(df))).reshape(-1,1)), axis=1)
    elif point_cloud_scan_file.split('.')[-1] == 'txt':
        df = pd.read_csv(root+sub+'/'+point_cloud_scan_file, header=None, sep=' ')
        new_scan = np.concatenate((df[df.columns[0]].values.reshape(-1,1), 
                        df[df.columns[1]].values.reshape(-1,1),
                        df[df.columns[2]].values.reshape(-1,1),
                        np.array([0]*len(df)).reshape(-1,1),
                        np.array([0]*len(df)).reshape(-1,1),
                        np.array([0]*len(df)).reshape(-1,1),
                        np.array([0]*len(df)).reshape(-1,1)), axis=1)
    print ("scan:", point_cloud_scan_file, new_scan.shape)
    if full_scan is None:
        full_scan = new_scan
    else:
        full_scan = np.concatenate((full_scan, new_scan), axis=0)
print ("total scan:", full_scan.shape)


pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(full_scan[:,0:3])
xyz = np.asarray(pcd.points)
np.savetxt(sub+".csv", xyz, delimiter=",")

#o3d.io.write_point_cloud(sub+".csv", pcd)

asd


#BOF visualise all points
# pcd = o3d.geometry.PointCloud()
# df = pd.DataFrame(full_scan)
# pcd.points = o3d.utility.Vector3dVector(df[[df.columns[0], df.columns[1], df.columns[2]]].values)
#
# #normalise in range a-b
# df[df.columns[3]] = (0.9-0.1)*(df[df.columns[3]].values-df[df.columns[3]].min())/(df[df.columns[3]].max()-df[df.columns[3]].min())+0.1
#
# intensity = np.zeros((len(full_scan),3))
# intensity[:,0] = np.reshape(df[[df.columns[3]]].values,-1)
# intensity[:,1] = np.reshape(df[[df.columns[3]]].values,-1)
# intensity[:,2] = np.reshape(df[[df.columns[3]]].values,-1)
# pcd.colors = o3d.utility.Vector3dVector(intensity)
#
# #pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
# o3d.visualization.draw_geometries([pcd])
#EOF visualise all points

def evaluate(df, vertex_normals):
    combined = pd.concat([df[[df.columns[4], df.columns[5], df.columns[6]]], pd.DataFrame(np.asarray(vertex_normals), columns=None)], axis=1)
    result = combined.apply(lambda row : angle_between((row[0], row[1], row[2]), (row[4], row[5], row[6])), axis=1)
    return [np.mean(result), min(result), max(result)]

def angle_between(v1, v2):
    def unit_vector(vector):
        """ Returns the unit vector of the vector.  """
        return vector / np.linalg.norm(vector)
        
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.degrees(np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)))

def get_nn_distances(pcd):
    # heuristic to estimate the radius of a rolling ball
    distances = pcd.compute_nearest_neighbor_distance() #compute_nearest_neighbor_distance(self) - Function to compute the distance from a point to its nearest neighbor in the point cloud
    return distances
    

def ball_pivoting_reconstruction(pcd, normals_search_param, radii, voxel_size=None):
    
    if voxel_size is not None:
        print ("DOWNSAMPLING")
        pcd = pcd.voxel_down_sample(voxel_size=voxel_size)
    
    pcd.estimate_normals(search_param=normals_search_param)
    
    mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pcd, o3d.utility.DoubleVector(radii))
    
    return mesh
    
    
def poisson_reconstruction(pcd, normals_search_param, depth, width, scale, linear_fit, voxel_size=None):
    
    if voxel_size is not None:
        print ("DOWNSAMPLING")
        pcd = pcd.voxel_down_sample(voxel_size=voxel_size)
    
    pcd.estimate_normals(search_param=normals_search_param)

    distances = pcd.compute_nearest_neighbor_distance() #compute_nearest_neighbor_distance(self) - Function to compute the distance from a point to its nearest neighbor in the point cloud
    
    mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd, depth=depth, width=width, scale=scale, linear_fit=linear_fit)
    
    return mesh
    
best_mean = [180, -1, -1, -1]
#BOF ball pivoting
if True:#for ball_pivoting_max_nn in range(1, 6):
    #print ("ball_pivoting_max_nn", ball_pivoting_max_nn)
    if True:#for ball_pivoting_normals_radius_multiplier in np.arange(0.01, 5, 0.3):
        #print ("ball_pivoting_normals_radius_multiplier", ball_pivoting_normals_radius_multiplier)
        if True:#for _ in range(0,5):

            df = pd.DataFrame(full_scan)
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(df[[df.columns[0], df.columns[1], df.columns[2]]].values)
            distances = get_nn_distances(pcd)

            ball_pivoting_max_nn = 2 # lower - more faces
            ball_pivoting_normals_radius_multiplier = 2 # no effect?
            ball_pivoting_radii_multiplier = [3.0, 5.0] # low/high - more disconnects too little: [0.5, 1.0] - ok: [1.5, 2.0] - too much: [3.0 - 5.0] 
            # ball_pivoting_voxel_size = 0.03
            
            #import random
            #from random import randrange
            #alist = np.arange(0.01, 5, 0.5)
            #ball_pivoting_radii_multiplier = random.sample(list(alist), randrange(len(list(alist))))

            normals_search_param = o3d.geometry.KDTreeSearchParamHybrid(radius=np.mean(distances) * ball_pivoting_normals_radius_multiplier,
                                                                        max_nn=ball_pivoting_max_nn)
            mesh = ball_pivoting_reconstruction(pcd, 
                                                normals_search_param,
                                                radii=[np.mean(distances)*multiplier for multiplier in ball_pivoting_radii_multiplier])
                                                #voxel_size=ball_pivoting_voxel_size)

            #check if many rows are 0s
            #print(np.where(np.all(np.isclose(np.asarray(mesh.vertex_normals), 0), axis=1)))
            
            mean, minimum, maximum = evaluate(df, mesh.vertex_normals)
            #print ("ball_pivoting_max_nn", ball_pivoting_max_nn, "ball_pivoting_normals_radius_multiplier", ball_pivoting_normals_radius_multiplier, "ball_pivoting_radii_multipliers", str(ball_pivoting_radii_multipliers),
            #        "mean", mean, "min", minimum, "max", maximum)
            
            if mean < best_mean[0]:
                best_mean[0] = mean
                best_mean[1] = ball_pivoting_max_nn
                best_mean[2] = ball_pivoting_normals_radius_multiplier
                best_mean[3] = str(ball_pivoting_radii_multiplier)
                
                print (best_mean)

mesh = trimesh.Trimesh(np.asarray(mesh.vertices), np.asarray(mesh.triangles), vertex_normals=np.asarray(mesh.vertex_normals))
#mesh.fix_normals()
mesh.export(sub+'_ball_pivoting.obj')
#EOF ball pivoting

#BOF poisson
# df = pd.DataFrame(full_scan)
# pcd = o3d.geometry.PointCloud()
# pcd.points = o3d.utility.Vector3dVector(df[[df.columns[0], df.columns[1], df.columns[2]]].values)
# distances = get_nn_distances(pcd)
#
# poisson_max_nn = 12
# poisson_depth = 10
# poisson_width = 0
# poisson_scale = 1
# poisson_linear_fit = True
# poisson_voxel_size = 0.03
#
# normals_search_param = o3d.geometry.KDTreeSearchParamHybrid(radius=np.mean(distances),
#                                                             max_nn=poisson_max_nn)
# mesh = poisson_reconstruction(pcd,
#                             normals_search_param,
#                             depth=poisson_depth,
#                             width=poisson_width,
#                             scale=poisson_scale,
#                             linear_fit=poisson_linear_fit,
#                             voxel_size=poisson_voxel_size)
# mesh = trimesh.Trimesh(np.asarray(mesh.vertices), np.asarray(mesh.triangles), vertex_normals=np.asarray(mesh.vertex_normals))
# #mesh.fix_normals()
# mesh.export(sub+'_poisson.obj')
#EOF poisson

#mesh.show()